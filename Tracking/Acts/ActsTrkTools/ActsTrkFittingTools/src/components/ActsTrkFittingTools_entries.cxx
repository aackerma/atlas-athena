/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/


#include "../ActsKalmanFitter.h"
#include "../ActsGaussianSumFitter.h"

DECLARE_COMPONENT( ActsKalmanFitter )
DECLARE_COMPONENT( ActsGaussianSumFitter )

